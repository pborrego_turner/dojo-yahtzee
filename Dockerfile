FROM node:latest

# Build environment variables
ENV APP_PATH=/home/app
ENV APP=$APP_PATH/yahtzee

# Set up non root user to run install and build
RUN useradd --user-group --create-home --shell /bin/false app

# Set up folder and add install files
RUN mkdir -p $APP
COPY package.json $APP

# Any kind of copying from the host must be down as root so this sets
# the permissions for the app user to be able to read the files
RUN chown -R app:app $APP_PATH/*

USER app
WORKDIR $APP

# Install app dependencies
RUN npm install && npm cache clean

CMD ["npm", "test"]
