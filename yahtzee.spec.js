const expect = require('expect');
const yahtzee = require('./yahtzee');

describe('Yahtzee!', function () {
    it('should exist', function () {
        expect(yahtzee).toExist();
    });

   

    it('single count', function() {
        expect(yahtzee([1, 1, 2, 4, 4], 'single-count', 4)).toBe(8);
    });
    it('single pairs', function() {
        expect((yahtzee([3, 3, 3, 4, 4], 'single-pairs'))).toBe(8);
    });
    it('3 of a kind', function() {
        expect((yahtzee([3, 3, 3, 4, 5], 'three-kind'))).toBe(9);
        expect((yahtzee([1,2,3,4,5], 'three-kind'))).toBe(0);
    });
    it('four of a kind', function() {
        expect((yahtzee([2, 2, 2, 2, 5], 'four-kind'))).toBe(8);
    });
    it('yahtzee', function() {
        expect((yahtzee([1, 1, 1, 1, 1], 'yahtzee'))).toBe(50);
        expect((yahtzee([1, 1, 1, 1, 2], 'yahtzee'))).toBe(0);
    });
    it('chance', function() {
        expect(yahtzee([2, 6, 4, 3, 6], 'chance')).toBe(21);
    });
    it('small straight');
    it('large straight');
    it('full house');

});
