## Dojo Yahtzee

Small project to solve this: http://codingdojo.org/kata/Yahtzee/

## Prerequirements
[Docker](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/install/)

## Install and Startup

This runs the solution.

```
docker-compose up
```
