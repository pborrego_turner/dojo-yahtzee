function roll(dice, category, optValue) {
    switch(category) {
        case 'chance':
            return sum(dice);
        case 'single-count':
            return singleCount(dice, optValue);
        case 'single-pairs':
            return pairs(dice, 2);
        case 'three-kind':
            return pairs(dice, 3);
        case 'four-kind':
            return pairs(dice, 4);
        case 'yahtzee':
            return pairs(dice, 5) && 50
        

    }
}

function sum(dice) {
    return dice.reduce( (prev, curr) => prev + curr, 0);
}

module.exports = roll;

function singleCount(dice, value) {
    return sum(dice.filter( (val) => val === value));
}

function pairs(dice, howMany) {
    let sorted = dice.sort();
    return sorted.reduce( (prev, curr, index, arr) => {
        const lookAhead = arr.slice(index, index + howMany);
        const eq = isNthKind(lookAhead, howMany, curr);

        return (eq && [...prev, lookAhead]) || prev;

    }, [[0]]).map( (v => sum(v))).pop();
}


function isEqual(dice, val) {
    return dice.every( (v) => v === val);
}

function isNthKind(arr, len, value) {
    return arr.length === len && isEqual(arr, value);
}